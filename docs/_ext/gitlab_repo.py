from enum import StrEnum
from typing import TYPE_CHECKING

from docutils import nodes
from sphinx.domains import Domain
from sphinx.errors import ExtensionError
from sphinx.roles import ReferenceRole, SphinxRole
from sphinx.transforms import SphinxTransform

if TYPE_CHECKING:
    from sphinx.application import Sphinx
    from sphinx.config import Config


class ConfigValues(StrEnum):
    BPROTO_REPO_LINK = "bproto_repo_link"
    REPO_LINK = "repo_link"


def _make_url(title: str, url: str) -> nodes.reference:
    if not title:
        title = url

    return nodes.reference("", title, internal=False, refuri=url)


class BaseRepoRole(ReferenceRole):
    config_name: str
    url_part: str

    def run(self) -> tuple[list[nodes.Node], list[nodes.system_message]]:
        base_url: str = getattr(self.config, self.config_name)
        refuri = f"{base_url}/-/{self.url_part}/{self.target}"

        return [_make_url(self.title, refuri)], []


# bproto


class BProtoCommitRole(BaseRepoRole):
    config_name = ConfigValues.BPROTO_REPO_LINK
    url_part = "commit"


class BProtoIssueRole(BaseRepoRole):
    config_name = ConfigValues.BPROTO_REPO_LINK
    url_part = "issues"


class BProtoUrlRole(SphinxRole):
    config_name = ConfigValues.BPROTO_REPO_LINK

    def run(self) -> tuple[list[nodes.Node], list[nodes.system_message]]:
        refuri = getattr(self.config, self.config_name)
        return [_make_url(self.text, refuri)], []


# In Shpinx we can define ":bproto-url:`title`". However ":bproto-url:" is an
# invalid use of Sphinx role. To overcome that we also define a "|bproto-url|"
# substitution, which might be quite handy when we just want to get a repo link.
class BProtoUrlSubstitutions(SphinxTransform):
    config_name = ConfigValues.BPROTO_REPO_LINK

    # run before the default Substitutions
    default_priority = 210

    def apply(self, **kwargs: dict) -> None:
        refuri = getattr(self.config, self.config_name)
        node = _make_url("", refuri)

        for ref in self.document.findall(nodes.substitution_reference):
            if ref["refname"] == "bproto-url":
                ref.replace_self(node)


class BProtoDomain(Domain):
    name = "bproto"
    label = "bproto repo links"
    roles = {
        "commit": BProtoCommitRole(),
        "issue": BProtoIssueRole(),
        "url": BProtoUrlRole(),
    }


# repo


class RepoFileRole(BaseRepoRole):
    config_name = ConfigValues.REPO_LINK
    url_part = "tree/main"


class RepoDomain(Domain):
    name = "repo"
    label = "gitlab repo links"
    roles = {
        "file": RepoFileRole(),
    }


# setup


def _handle_inited_config(app: "Sphinx", config: "Config") -> None:
    for config_name in ConfigValues:
        if not getattr(config, config_name.value):
            raise ExtensionError(f"{config_name.value!r} config value is empty!")


def setup(app: "Sphinx"):
    for config_name in ConfigValues:
        app.add_config_value(config_name.value, None, "env", [str])

    app.connect("config-inited", _handle_inited_config)

    app.add_domain(BProtoDomain)
    app.add_transform(BProtoUrlSubstitutions)

    app.add_domain(RepoDomain)

    return {
        "version": "0.0.0",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
