============
Useful Links
============

CI/CD
=====

**.gitlab-ci.yml**
    Official GitLab documentation for ``.gitlab-ci.yml`` keyword reference.

    Available online at https://docs.gitlab.com/ee/ci/yaml/.

**GitLab CI/CD pipelines**
    Official GitLab documentation for managing GitLab CI/CD pipelines.

    Available online at https://docs.gitlab.com/ee/ci/pipelines.

**GitLab CI/CD jobs**
    Official GitLab documentation for managing GitLab CI/CD jobs.

    Available online at https://docs.gitlab.com/ee/ci/jobs.

**GitLab CI/CD variables**
    Official GitLab documentation for managing GitLab CI/CD variables.

    Available online at https://docs.gitlab.com/ee/ci/variables.

**GitLab CI/CD pre-defined variables**
    Official GitLab documentation for managing GitLab CI/CD predefined variables.

    Available online at https://docs.gitlab.com/ee/ci/variables/predefined_variables.html.

**GitLab CI/CD caching**
    Official GitLab documentation for using caching in GitLab CI/CD.

    Available online at https://docs.gitlab.com/ee/ci/caching.

Useful artifacts
================

**GitLab CI/CD artifacts reports types**
    Official list of all available GitLab CI/CD artifacts reports types.

    Available online at https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html.

**Code Quality**
    Official GitLab documentation for managing code quality reports.

    Available online at https://docs.gitlab.com/ee/ci/testing/code_quality.html.

**Test coverage**
    Official GitLab documentation for managing test coverage reports.

    Available online at https://docs.gitlab.com/ee/ci/testing/test_coverage_visualization.html.

**Unit tests**
    Official GitLab documentation for managing unit tests reports.

    Available online at https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html.

Pages
=====

**GitLab Pages**
    Official GitLab documentation for managing GitLab pages.

    Available online at https://docs.gitlab.com/ee/user/project/pages.
