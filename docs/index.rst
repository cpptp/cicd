=========================
GitLab Common CI/CD Tools
=========================

.. toctree::
   :caption: Documentation
   :maxdepth: 1
   :hidden:

   overview/index.rst
   images/index.rst
   pipelines/index.rst
   fork.rst
   variables.rst
   useful_links.rst

.. toctree::
   :caption: Development
   :maxdepth: 1
   :hidden:

   git/index.rst

.. _fork: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

.. _`Bytes Mess`: https://gitlab.com/b110011

**GitLab Common CI/CD Tools** (**GitLab 3CT** for short) provides a `GitLab CI/CD <https://docs.gitlab.com/ee/ci>`__ solution that helps to control the quality of mainly C++ and Python codebases and their surrounding infrastructure.

While the driving force of this module is to reduce duplication of CI/CD configurations and scripts across projects developed by me (`Bytes Mess`_), it is intended to be useful for any software that wants to utilize GitLab CI/CD to some degree.

This module also may serve as a useful reference for setting up a GitLab CI/CD using good practices.

Usage
=====

By design this project is intended to be used as a `CI/CD template <https://docs.gitlab.com/ee/ci/examples/index.html>`__ in most of the cases.
However due to some limitations it might be `forked <fork_>`__ to bring some changes and then to be used as your own CI/CD template.
Both ways will be covered in-deep down on this page.

CI/CD templates
---------------

This project provides a bunch of useful `GitLab CI/CD pipelines <https://docs.gitlab.com/ee/ci/pipelines/index.html>`__ for different programming languages and frameworks.
Below is a nice list of all available pipelines:

- **Documentation**
    - Hugo (:repo:file:`.gitlab/ci/external/hugo.yml`)
    - Sphinx (:repo:file:`.gitlab/ci/external/sphinx.yml`)
- **Languages**
    - C++ (:repo:file:`.gitlab/ci/external/cpp.yml`)
    - CMake (:repo:file:`.gitlab/ci/external/lite.yml`)
    - Docker (:repo:file:`.gitlab/ci/external/lite.yml`)
    - Linux shell (:repo:file:`.gitlab/ci/external/lite.yml`)
    - Python (:repo:file:`.gitlab/ci/external/lite.yml`)
    - YAML (:repo:file:`.gitlab/ci/external/lite.yml`)
- **Packages**
    - Conan (:repo:file:`.gitlab/ci/external/conan.yml`)

You can start using those pipelines by including one of them as shown below:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'

You also need to define below `CI/CD variables <https://docs.gitlab.com/ee/ci/variables>`__ in your `group <https://docs.gitlab.com/ee/ci/variables/#for-a-group>`__ or `project <https://docs.gitlab.com/ee/ci/variables/#for-a-project>`__ CI/CD settings.

.. raw:: html

   <style>
   table {
     white-space: nowrap;
   }
   </style>

+----------+---------------------------+------------------------------------------+-----------+---------+--------------+
| Type     | Key                       | Value                                    | Protected | Masked  | Environments |
+==========+===========================+==========================================+===========+=========+==============+
| Variable | BPROTO_CI_IMAGES_REGISTRY | ``registry.gitlab.com/bproto/artifacts`` | no        | no      | All          |
+----------+---------------------------+------------------------------------------+-----------+---------+--------------+

Now you are ready to go!

.. note::

   To have a better understanding of pipelines implementation, you might also consider looking at an :ref:`overview <overview>` of different aspects of pipelines, such as a list of implemented :ref:`stages <overview-stages>` and how the :ref:`workflow <overview-workflow>` was designed.

Forks
-----

If you think, you need to fork this project and adjust it to your personal needs, please consider taking a look at :ref:`this documentation page <forking-project>`.

License
=======

All code is licensed under the `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`__ license.
