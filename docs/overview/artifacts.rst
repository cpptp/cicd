.. _overview-artifacts:

=========
Artifacts
=========

.. _container-registry: https://docs.gitlab.com/ee/user/packages/container_registry/index.html
.. _package-registry: https://docs.gitlab.com/ee/user/packages/package_registry/index.html

An artifacts system for this project is fully based on GitLab `package <package-registry_>`_ and `container <container-registry_>`_ registries and tries to utilize them to the fullest.

It's still possible to use `Docker Hub <https://hub.docker.com>`__ and `JFrog's Artifactory <https://jfrog.com/artifactory>`__ but that will require some work to be done.

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/artifacts.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_images/artifacts.png
   :alt: Artifacts main repository
   :class: only-light

.. image:: /_images/artifacts.dark.png
   :alt: Artifacts main repository
   :class: only-dark

Also despite the fact that any GitLab project can enable `package <package-registry_>`_ and `container <container-registry_>`_ registries, it has been decided to store all "shared" artifacts in a separate GitLab project called "artifacts".

Here is an example of a such one: https://gitlab.com/bproto/artifacts.

This approach helps to provide emulate a single source of artifacts and provide a simple way of distinguishing a "local" project only artifacts from those that are supposed to be shared across other projects too.
