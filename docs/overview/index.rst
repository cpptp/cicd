.. _overview:

========
Overview
========

This page is geared towards developers of the project CI/CD pipelines, and serves as a starting point of understanding core parts of pipelines.

.. toctree::
   :maxdepth: 1
   :hidden:

   stages.rst
   workflow.rst
   artifacts.rst

:doc:`stages`
    An exhaustive overview of ideas behind pipeline stages.

:doc:`workflow`
    An overview of triggers for jobs from different stages.

:doc:`artifacts`
    An overview of project ideas around artifacts storage.
