.. _overview-workflow:

========
Workflow
========

Stages distribution
===================

A below table shows when jobs of a certain stage will be triggered.

.. table::
   :width: 100%

   +--------+---------+-------------------+---------------+
   | Stage  | Branch  | CI_DEFAULT_BRANCH | Merge request |
   +========+=========+===================+===============+
   | build  | no      | no                | **yes**       |
   +--------+---------+-------------------+---------------+
   | test   | no      | no                | **yes**       |
   +--------+---------+-------------------+---------------+
   | check  | no      | no                | **yes**       |
   +--------+---------+-------------------+---------------+
   | report | no      | no                | **yes**       |
   +--------+---------+-------------------+---------------+
   | deploy | no      | **yes**           | no            |
   +--------+---------+-------------------+---------------+
   | misc   | no      | **yes**           | no            |
   +--------+---------+-------------------+---------------+

Merge request labels
====================

.. _merge requests: https://docs.gitlab.com/ee/user/project/merge_requests/
.. _label: https://docs.gitlab.com/ee/user/project/labels.html

Current implementation provides a way to temporary or completely disable pipelines for specific `merge requests`_ by setting special label_.
Below is a list of such labels.

.. note::
    Do please note that everything will work even you add only one label from the list.

.. list-table::
    :header-rows: 1

    * - Name
      - Description

    * - ``ci::skip``
      -

    * - ``workflow::paused``
      - Merge requests / issues which are temporarily not worked on

Also it's recommended, but not required, to add other useful labels.
However if you have already set-up your own labels you can safely skip this part.

.. list-table::
    :header-rows: 1

    * - Name
      - Description

    * - ``workflow::blocked``
      - Merge requests / issues that are blocked until another issue has been completed

    * - ``workflow::in-development``
      - Merge requests / issues that are actively being worked on by a developer
