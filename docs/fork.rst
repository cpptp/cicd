.. _forking-project:

===============
Forking project
===============

.. _caching: https://docs.gitlab.com/ee/ci/caching
.. _fork: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

The general rule of thumb can be answering positive to one of below questions:

- do I need a different toolchain than is provided by current :ref:`pre-build docker image <images>`?
- do I need a different set of python package than what is provided by current :ref:`pre-build docker image <images>` and caching_ can't help you with that?
- do I need to tweak :ref:`C++ pipeline <pipelines-lang-cpp>` implementation?

Also it's worth to mention that some pipelines, like :ref:`C++ <pipelines-lang-cpp>` one, have :ref:`though limitations <pipelines-lang-cpp-limitations>` that can be overcome by caching_ packages but you still might decide to `fork <fork_>`__ this project and change some lines.

.. note::

    Cookbook for GitLab forks can be found in `official documentation <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>`__.
    There you can find how to fork a project and how to maintain it.

TODO list
=========

Mandatory actions
-----------------

#. Create a separate git branch.

   .. note::

      It's worth to mention - this is a rather personal choice.
      However, if you are not planning to integrate your changes into :bproto:url:`parent project`, it's better to have a separate branch which contains only your changes.

#. Change project path in :repo:file:`CI external files <.gitlab/ci/external>` from ``bproto/cicd`` to your own.

#. Change macro prefix :repo:file:`clang-tidy config <.clang-tidy>` file from ``BPROTO`` to your own.

#. Change include guards prefix from ``BPROTO`` to your own in an appropriate :repo:file:`shell script <scripts/cicd/lint/check_cpp_include_guards.sh>`.

#. Add project specific python packages to :repo:file:`pyproject.toml <pyproject.toml>`.

#. Update :repo:file:`conan profiles <conan/profiles>` accordingly to your needs.

   .. note:: Current profiles set ``c++17`` standard.

#. Add project specific conan packages to :repo:file:`conanfile.txt <conan/conanfile.txt>`.

Optional actions
----------------

#. Update ``clang-format`` :repo:file:`config file <.clang-format>` to your needs.

#. Update ``clang-tidy`` :repo:file:`config file <.clang-tidy>` to your needs.

Automation
==========

There is a simple shell script that can automate some of mentioned actions.
You can find it under :repo:file:`scripts/fork.sh` path.

If you've forked :bproto:url:`bproto/cicd` into https://gitlab.com/supercoder/cicd, you can run script as follows:

.. code-block:: bash

   bash scripts/fork.sh -p MY_PREFIX -r supercoder/cicd

More information you can get by reading the script code, which is pretty straight forward.
