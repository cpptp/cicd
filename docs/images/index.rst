.. _images:

=======================
Pre-build docker images
=======================

This section provides an information about all custom docker images, which are developed and maintained within this project.

General purpose
===============

Here is listed all docker images with a very general purpose setup, that can be used in wide range of use-cases:

.. toctree::
   :caption: General purpose
   :hidden:
   :maxdepth: 1

   g/index.rst

- :doc:`g/alpine3`,
- :doc:`g/centos7`,
- :doc:`g/centos8`,
- :doc:`g/ubuntu2004`,
- :doc:`g/ubuntu2304`,
- :doc:`g/windows`.

Dedicated
=========

Here is listed all docker images with a very limited setup, that were created to solve a very specific set of problems:

.. toctree::
   :caption: Dedicated
   :hidden:
   :maxdepth: 1

   d/index.rst

- :doc:`d/clang-format`,
- :doc:`d/conan`,
- :doc:`d/hadolint`.
