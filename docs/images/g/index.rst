======================
General purpose images
======================

Here is listed all docker images with a very general purpose setup, that can be used in wide range of use-cases:

.. toctree::
   :hidden:
   :maxdepth: 1

   alpine3.rst
   centos7.rst
   centos8.rst
   ubuntu2004.rst
   ubuntu2304.rst
   windows.rst

- :doc:`alpine3`,
- :doc:`centos7`,
- :doc:`centos8`,
- :doc:`ubuntu2004`,
- :doc:`ubuntu2304`,
- :doc:`windows`.
