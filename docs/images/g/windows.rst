.. _images-general-windows:

=========================
Windows ServerCore (1809)
=========================

A C++ Windows-based image which contains all required and optional packages to build C++ projects based on CMake build system.

.. note:: As this system is currently a main one, it can be used as a reference to setup your own C/C++ workspace on Windows.

*TAGS*

- ``windows-base:latest`` - contains all necessary packages for build C++ project.
- ``windows:latest`` - contains pre-build ``conan`` packages.

*BASE IMAGE*
    Official `windows/servercore <https://hub.docker.com/_/microsoft-windows-servercore>`__ image.

*BUILD TIME*

- ``windows-base:latest`` - 100 minutes on average.
- ``windows:latest`` - 45 minutes on average.

*PULL TIME*

- Around 5 minutes to setup virtual machine.
- Around 15 minutes to pull ``windows:latests`` image.

*SYSTEM PACKAGES*

.. caution:: Package versions dated by 13 January 2024.

+------------------------+---------+------------------------------------------------------------------------+
| Package                | Version | Purpose                                                                |
+========================+=========+========================================================================+
| .. centered:: **Compilation**                                                                             |
+------------------------+---------+------------------------------------------------------------------------+
| ``cmake``              | 3.27.0  | To provide a C++ build system.                                         |
+------------------------+---------+------------------------------------------------------------------------+
| ``Visual Studio 2022`` | \-      | To provide a C++ compiler.                                             |
+------------------------+---------+------------------------------------------------------------------------+
| .. centered:: **Essential**                                                                               |
+------------------------+---------+------------------------------------------------------------------------+
| ``choco``              | 1.4.0   | To provide simple way of installing Windows packages.                  |
+------------------------+---------+------------------------------------------------------------------------+
| ``git``                | \-      | To been able to download GitLab project in a job container.            |
+------------------------+---------+------------------------------------------------------------------------+
| ``python3``            | 3.8     | To provide support of ``python`` language and bring a packages manager |
+------------------------+---------+------------------------------------------------------------------------+

*C++ PACKAGES*
    Only those which are listed in :repo:file:`conan/conanfile.txt` file.

*PYTHON PACKAGES*
    Only :repo:file:`conan <pyproject.toml>` packages.

*CI SCRIPTS*
    No scripts are included.
