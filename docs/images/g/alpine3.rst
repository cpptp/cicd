.. _images-general-alpine3:

========
Alpine 3
========

A utility image which contains all possible linters and ``sphinx`` related packages, therefore widely used in documentation and linters jobs.

*TAGS*

- ``alpine3:latest``

*BASE IMAGE*
    Official `python:3.12-alpine <https://hub.docker.com/_/alpine>`_ image.

*BUILD TIME*
    2 minutes on average.

*SYSTEM PACKAGES*

.. caution:: Package versions dated by 29 March 2023.

+-----------------+---------+-------------------------------------------------------------+
| Package         | Version | Purpose                                                     |
+=================+=========+=============================================================+
| .. centered:: **Essential**                                                             |
+-----------------+---------+-------------------------------------------------------------+
| ``git``         | \-      | To been able to download GitLab project in a job container. |
+-----------------+---------+-------------------------------------------------------------+
| .. centered:: **Utils**                                                                 |
+-----------------+---------+-------------------------------------------------------------+
| ``bat``         | \-      | To provided syntax highlighting in GitLab terminal.         |
+-----------------+---------+-------------------------------------------------------------+
| ``curl``        | \-      | Mainly to upload packages to GitLab.                        |
+-----------------+---------+-------------------------------------------------------------+
| ``less``        | \-      | To update ``less`` and bring support for additional flags.  |
+-----------------+---------+-------------------------------------------------------------+
| .. centered:: **Linters & Sanitizers**                                                  |
+-----------------+---------+-------------------------------------------------------------+
| ``shellcheck``  | \-      | To provided a shell scripts linter.                         |
+-----------------+---------+-------------------------------------------------------------+
| ``shfmt``       | \-      | To provided a shell scripts formatter.                      |
+-----------------+---------+-------------------------------------------------------------+
| .. centered:: **Other**                                                                 |
+-----------------+---------+-------------------------------------------------------------+
| ``gcc``         | \-      | **Only to build** ``licenseheaders`` **linter.**            |
+-----------------+---------+-------------------------------------------------------------+
| ``musl-dev``    | \-      | **Only to build** ``licenseheaders`` **linter.**            |
+-----------------+---------+-------------------------------------------------------------+

*C++ PACKAGES*
    None.

*PYTHON PACKAGES*
    Mainly :repo:file:`docs and linters <pyproject.toml>` packages.
