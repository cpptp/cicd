.. _images-dedicated-hadolint:

========
HadoLint
========

A custom ``hadolint`` docker image with changed entrypoint and a bunch of new packages.

*TAGS*

- ``hadolint:latest``

*BASE IMAGE*
    Official `hadolint:v2.12.0-alpine <https://hub.docker.com/r/hadolint/hadolint>`_ image.

*BUILD TIME*
    1 minute on average.

*SYSTEM PACKAGES*

*Final* docker image will contain only below additional packages.

+------------------+---------+-------------------------------------------------------------+
| Package          | Version | Purpose                                                     |
+==================+=========+=============================================================+
| ``bat``          | \-      | To provided syntax highlighting in GitLab terminal.         |
+------------------+---------+-------------------------------------------------------------+
| ``bash``         | \-      | To provide support ``bash`` scripting language.             |
+------------------+---------+-------------------------------------------------------------+
| ``git``          | \-      | To been able to download GitLab project in a job container. |
+------------------+---------+-------------------------------------------------------------+
| ``hadolint``     | \-      | To provided a docker file linter.                           |
+------------------+---------+-------------------------------------------------------------+

*C++ PACKAGES*
    None.

*PYTHON PACKAGES*
    None.

*CI SCRIPTS*
    Only ``hadolint`` related CI scripts are included in this docker image.
