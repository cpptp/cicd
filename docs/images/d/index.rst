================
Dedicated images
================

Here is listed all docker images with a very limited setup, that were created to solve a very specific set of problems:

.. toctree::
   :hidden:
   :maxdepth: 1

   clang-format.rst
   conan.rst
   hadolint.rst

- :doc:`clang-format`,
- :doc:`conan`,
- :doc:`hadolint`.
