.. _images-dedicated-conan:

=====
Conan
=====

A custom ``conan`` docker image with a limited amount of packages.

This docker image is provided as a clean setup for testing and deployment of ``conan`` packages that resides in `bPROTO recipes <https://gitlab.com/bproto/recipes>`__ group.

*TAGS*

- ``conan:latest``

*BASE IMAGE*
    Official `ubuntu:23.04 <https://hub.docker.com/_/ubuntu>`__ image.

*BUILD TIME*
    1 minute on average.

*SYSTEM PACKAGES*

*Final* docker image will contain only below additional packages.

.. caution:: Package versions dated by 1 January 2024.

+------------------+---------+--------------------------------------------------------------------------------+
| Package          | Version | Purpose                                                                        |
+==================+=========+================================================================================+
|  .. centered:: **Compilation**                                                                              |
+------------------+---------+--------------------------------------------------------------------------------+
| ``g++-13``       | 13.4.0  | To provide a C compiler.                                                       |
+------------------+---------+--------------------------------------------------------------------------------+
| ``gcc-13``       | 13.4.0  | To provide a C++ compiler.                                                     |
+------------------+---------+--------------------------------------------------------------------------------+
| ``ldd``          | 2.37    | ``glibc`` version.                                                             |
+------------------+---------+--------------------------------------------------------------------------------+
| ``make``         | \-      | \-                                                                             |
+------------------+---------+--------------------------------------------------------------------------------+
| .. centered:: **Essential**                                                                                 |
+------------------+---------+--------------------------------------------------------------------------------+
| ``git``          | \-      | To been able to download GitLab project in a job container.                    |
+------------------+---------+--------------------------------------------------------------------------------+
| ``python3-pip``  | 23.0.1  | To bring a python packages manager.                                            |
+------------------+---------+--------------------------------------------------------------------------------+
| ``python3-venv`` | 3.11.2  | To overcome "externally managed environments" introduced by "PEP 668".         |
+------------------+---------+--------------------------------------------------------------------------------+
| .. centered:: **Utils**                                                                                     |
+------------------+---------+--------------------------------------------------------------------------------+
| ``doxygen``      | \-      | To provide a documentation generator for code which uses C/C++ code.           |
+------------------+---------+--------------------------------------------------------------------------------+

*C++ PACKAGES*
    None.

*PYTHON PACKAGES*
    Mainly :repo:file:`conan and docs <pyproject.toml>` packages.

*CI SCRIPTS*
    No scripts are included.
