.. _images-dedicated-clangformat:

============
Clang-Format
============

A docker image with a patched ``clang-format`` to support a custom C++ code style.

.. _alpine-image: https://hub.docker.com/_/alpine

*TAGS*

- ``clang-format:latest``

*BASE IMAGE*
    Official `alpine:3.18 <alpine-image_>`_ and `python:3-alpine3.18 <alpine-image_>`_ images (multi-stage build).

*BUILD TIME*
    20 minutes on average.

*SYSTEM PACKAGES*

*Final* docker image will contain only below additional packages.

+------------------+---------+-------------------------------------------------------------+
| Package          | Version | Purpose                                                     |
+==================+=========+=============================================================+
| ``bat``          | \-      | To provided syntax highlighting in GitLab terminal.         |
+------------------+---------+-------------------------------------------------------------+
| ``bash``         | \-      | To provide support ``bash`` scripting language.             |
+------------------+---------+-------------------------------------------------------------+
| ``clang-format`` | \-      | To provided a C/C++ formatter.                              |
+------------------+---------+-------------------------------------------------------------+
| ``git``          | \-      | To been able to download GitLab project in a job container. |
+------------------+---------+-------------------------------------------------------------+

*C++ PACKAGES*
    None.

*PYTHON PACKAGES*
    None.

*CI SCRIPTS*
    Only ``clang-format`` related CI scripts are included in this docker image.
