=========
Variables
=========

Here are all project related CI/CD variables that are used inside our GitLab pipeline, along with a brief explanation.

Local CI/CD variables
=====================

**CI_DEBUG_TRACE**:STRING
  Predefined GitLab CI variable that enables `debug logging (tracing) <https://docs.gitlab.com/ee/ci/variables/index.html#debug-logging>`__ to help troubleshoot problems with pipeline configuration or job scripts.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.

  .. warning::
    Debug logging can be a serious security risk.
    The output contains the content of all variables and other secrets available to the job.
    The output is uploaded to the GitLab server and visible in job logs.

.. _variables-bproto-ci-conan-package-name:

**BPROTO_CI_CONAN_PACKAGE_NAME**:STRING
  A name of a conan package which will be used when deploying to the :ref:`artifacts <overview-artifacts>` package registry.

**BPROTO_CI_LINTER_TRACE**:STRING
  Enable debug logging to help troubleshoot problems with linter jobs.
  Debug logging exposes job execution details that are usually hidden and makes job logs more verbose.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.

**BPROTO_CI_MERGE_REQUEST_CHANGED_FILES**:LIST
  A space-separated list of all files that have been added or changed.
  This list doesn't contain deleted files and files from 3rd-party folder.

  .. note:: This variable is only available for **linter** jobs.

.. _variables-bproto-ci-sphinx-source-dir:

**BPROTO_CI_SPHINX_SOURCE_DIR**:PATH
  Path to `Sphinx <https://www.sphinx-doc.org/en/master/>`__ source files.

  Defaults to ``docs``.

.. _variables-bproto-ci-sphinx-warnings-as-errors:

**BPROTO_CI_SPHINX_WARNINGS_AS_ERRORS**:BOOL
  Turn ``sphinx-build`` warnings into errors.

  Defaults to ``false``.

Global CI/CD variables
======================

**BPROTO_CI_CONAN_PACKAGES_REGISTRY**:URL
  A link to an :ref:`artifacts <overview-artifacts>`'s `conan packages <https://conan.io>`__ `registry <https://docs.gitlab.com/ee/user/packages/conan_repository>`__.

**BPROTO_CI_DEPLOY_TOKEN**:STRING
  A `deploy token <https://docs.gitlab.com/ee/user/project/deploy_tokens>`__ for :ref:`artifacts <overview-artifacts>`'s `packages and registries <https://docs.gitlab.com/ee/user/packages/>`__.

  **Scopes:** ``read_registry``, ``write_registry``, ``read_package_registry``, ``write_package_registry``.

**BPROTO_CI_DEPLOY_USER**:STRING
  A user for ``BPROTO_CI_DEPLOY_TOKEN``.

**BPROTO_CI_GENERIC_PACKAGES_REGISTRY**:URL
  A link to an :ref:`artifacts <overview-artifacts>`'s `generic packages repository <https://docs.gitlab.com/ee/user/packages/generic_packages>`__.

**BPROTO_CI_IMAGES_REGISTRY**:URL
  A link to an :ref:`artifacts <overview-artifacts>`'s `container registry <https://docs.gitlab.com/ee/user/packages/container_registry>`__.

**BPROTO_CI_PRIVATE_API_TOKEN**:STRING
  A `private token <https://docs.gitlab.com/ee/user/profile/personal_access_tokens>`__ of ``BPROTO_CI_USER`` with read-write access rights for API calls.

  **Scopes:** ``api``.

**BPROTO_CI_USER**:STRING
  An artificial user with maintainer rights.

GitLab group-level variables
----------------------------

+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Type     | Key                                 | Value                      | Protected | Masked  | Environments |
+==========+=====================================+============================+===========+=========+==============+
| Variable | BPROTO_CI_CONAN_PACKAGES_REGISTRY   | \-                         | **yes**   | no      | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_DEPLOY_TOKEN              | \-                         | no        | **yes** | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_DEPLOY_USER               | gitlab\+deploy\-token\-{n} | no        | **yes** | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_GENERIC_PACKAGES_REGISTRY | \-                         | **yes**   | no      | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_IMAGES_REGISTRY           | \-                         | no        | no      | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_PRIVATE_API_TOKEN         | \-                         | **yes**   | **yes** | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+
| Variable | BPROTO_CI_USER                      | \-                         | **yes**   | no      | All          |
+----------+-------------------------------------+----------------------------+-----------+---------+--------------+

.. note::
  To change above environment variables please follow instructions provided by the official `group CI/CD settings <https://docs.gitlab.com/ee/ci/variables/#for-a-group>`__ user guide.

Misc CI/CD variables
====================

**BPROTO_CI_ENABLE_PIPELINES_CLEANUP**:STRING
  Enable :ref:`misc-jobs-remove-old-pipelines` CI job in pipeline.
  This variable is typically used only on scheduled pipelines to enable only this specific job.

  Valid values: ``false`` and ``true``.

  Defaults to ``false``.
