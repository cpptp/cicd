=============
Miscellaneous
=============

This is a list of almost all available misc jobs that are currently run in our CI/CD pipelines.

Jobs overview
=============

.. _misc-jobs-remove-old-pipelines:

remove_old_pipelines
--------------------

This job executes a custom script that removes outdated pipelines from project pipeline list.

*ARTIFACTS*
    Job doesn't generate any artifacts.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1
    :width: 100%

    * - Name
      - Necessity
      - Origin

    * - BPROTO_CI_PRIVATE_API_TOKEN
      - Mandatory
      -

    * - BPROTO_CI_ENABLE_PIPELINES_CLEANUP
      - Mandatory
      -

    * - BPROTO_CI_GENERIC_PACKAGES_REGISTRY
      - Mandatory
      -

    * - *CI_API_V4_URL*
      - Mandatory
      - GitLab

    * - *CI_PROJECT_ID*
      - Mandatory
      - GitLab

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-misc-stage`.

*TRIGGERS*
    Job should be triggered only by a scheduler.

Scheduled job
^^^^^^^^^^^^^

For example GitLab scheduled job can be configured like this:

.. list-table::
    :header-rows: 1
    :width: 100%
    :widths: 40 60

    * - Name
      - Value

    * - Description
      - Clean all CI pipelines older than 1 week

    * - Interval Pattern
      - Custom (``0 0 * * 0``)

    * - Cron timezone
      - ``[UTC 0] UTC``

    * - Select target branch or tag
      - ``main``

    * - Variables
      - ``BPROTO_CI_ENABLE_PIPELINES_CLEANUP`` is ``true``

Usage
=====

In order to use above CI jobs, you need to add the follow code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
