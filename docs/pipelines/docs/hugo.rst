.. _pipelines-docs-hugo:

====
Hugo
====

This is a "sub-pipeline" for building and deploying new changes that have been made to `Hugo <https://gohugo.io/>`__ files of a specific GitLab project.

.. _pipelines-docs-hugo-triggers:

Triggers
========

.. caution::

    Right now all jobs from *Hugo "sub-pipeline"* stages are triggered on any change to repository.

Jobs overview
=============

build:docs
----------

This job is responsible for building HTML documentation from a clean state to make sure there are no unexpected results.

*ALLOW FAILURE*
    Job is not allowed to fail as any failure here means fundamental problems in documentation generation.

*ARTIFACTS*
    Job generate an artifact with a HTML documentation.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-build-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests**.

pages
-----

This stage is responsible for building and deploying static HTML documentation files to the project `GitLab Pages <https://docs.gitlab.com/ee/user/project/pages>`__.

*ALLOW FAILURE*
    Job is not allowed to fail as any failure here means fundamental problems in documentation generation.

*ARTIFACTS*
    Job generate an artifact with a HTML documentation.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-deploy-stage`.

*TRIGGERS*
    Job is triggered only on ``CI_DEFAULT_BRANCH``.
