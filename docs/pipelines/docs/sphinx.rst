.. _pipelines-docs-sphinx:

=============
Sphinx (reST)
=============

.. _gitlab-pages: https://docs.gitlab.com/ee/user/project/pages

This is a "sub-pipeline" for building and deploying new changes that have been made to `Sphinx <https://www.sphinx-doc.org>`__ files of a specific GitLab project.

.. note::

    Current approach is way more simple than the old one and solely relies on `GitLab Pages <gitlab-pages_>`__.
    If you still curious about how things worked before, you can look though :doc:`this page <sphinx_old_approach>` and investigate :bproto:commit:`this commit <e5c1d3c2bff66c592c17d2d5a2693b3a8b23101f>`.

.. _pipelines-docs-sphinx-environment:

Environment
===========

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Default value

    * - :ref:`BPROTO_CI_SPHINX_SOURCE_DIR <variables-bproto-ci-sphinx-source-dir>`
      - Optional
      - ``docs``

    * - :ref:`BPROTO_CI_SPHINX_WARNINGS_AS_ERRORS<variables-bproto-ci-sphinx-warnings-as-errors>`
      - Optional
      - ``false``

.. _pipelines-docs-sphinx-triggers:

Triggers
========

All jobs from *Sphinx "sub-pipeline"* stages are triggered and only if changes have been made to CMake files.
Right now such files are detected by these patterns:

- ``docs/**/*``
- ``project/**/CMakeLists.txt``
- ``project/**/include/**/*.hpp``

As you can see from the above list of files pipeline will take into account both types of the project documentation: Doxygen (C++ files) and Sphinx (RST files) documentation.

Jobs overview
=============

.. _pipelines-docs-sphinx-jobs-overview-build-docs:

build:docs
----------

This job is responsible for building HTML documentation from a clean state to make sure there are no unexpected results.

*ALLOW FAILURE*
    Job is not allowed to fail as any failure here means fundamental problems in documentation generation.

*ARTIFACTS*
    Job generate an artifact with a HTML documentation.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*
    A list of all environment variables can be found :ref:`here <pipelines-docs-sphinx-environment>`.

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-build-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-docs-sphinx-triggers>` is changed.

report:docs
-----------

This is a stage to simplify access to generated HTML files by :ref:`pipelines-docs-sphinx-jobs-overview-build-docs`.

*See also report stage overview from* :ref:`overview-stages-report-stage`.

pages
-----

This stage is responsible for building and deploying static HTML documentation files to the project `GitLab Pages <gitlab-pages_>`_.

*ALLOW FAILURE*
    Job is not allowed to fail as any failure here means fundamental problems in documentation generation.

*ARTIFACTS*
    Job generate an artifact with a HTML documentation.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*
    A list of all environment variables can be found :ref:`here <pipelines-docs-sphinx-environment>`.

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-deploy-stage`.

*TRIGGERS*
    Job is triggered only on ``CI_DEFAULT_BRANCH`` when any file from :ref:`trigger list <pipelines-docs-sphinx-triggers>` is changed.
