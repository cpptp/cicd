====
Docs
====

These documents cover in-detail all available "sub-pipelines" for documentation generation:

.. toctree::
   :maxdepth: 1
   :hidden:

   hugo.rst
   sphinx.rst

- :doc:`hugo`,
- :doc:`sphinx`.
