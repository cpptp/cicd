=========
Languages
=========

These documents cover in-detail all available "sub-pipelines" for different programming languages:

.. toctree::
   :maxdepth: 1
   :hidden:

   common.rst
   cmake.rst
   cpp/index.rst
   docker/index.rst
   python/index.rst
   shell.rst
   yaml.rst

- :doc:`common`,
- :doc:`cmake`,
- :doc:`cpp/index`,
- :doc:`docker/index`,
- :doc:`python/index`,
- :doc:`shell`,
- :doc:`yaml`.
