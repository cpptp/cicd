.. _pipelines-lang-yaml:

====
YAML
====

This is a "sub-pipeline" for linting new changes that have been made to YAML files of a specific GitLab project.

.. _pipelines-lang-yaml-triggers:

Triggers
========

All jobs from *YAML "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to YAML files.
Right now such files are detected by these patterns:

- ``**/*.yml``.

Jobs overview
=============

check:yaml:format
-----------------

This job executes `yamllint <https://github.com/adrienverge/yamllint>`__ *only on changed YAML files* (not diff, but files) which will diagnose and fix style violations.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.yamllint`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Type

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      - Public

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      - Internal

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-yaml-triggers>` is changed.

Usage
=====

In order to use above CI jobs, you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
