.. _pipelines-lang-docker:

======
Docker
======

This is a "sub-pipeline" for building, linting and deploying new changes that have been made to Docker files of a specific GitLab project.

.. toctree::
   :maxdepth: 1
   :hidden:

   build.rst
   linters.rst

:doc:`build`
    A document which covers all CI jobs responsible for building and deploying docker images.

:doc:`linters`
    A document which covers all CI jobs responsible for linting docker files.
