==============
Build & Deploy
==============

This is a pipeline for testing and deploying new changes that have been made to anything that is directly related to the *Docker* part of the project.

.. _pipelines-lang-docker-environment:

Environment
===========

.. list-table::
    :header-rows: 1
    :width: 100%

    * - Name
      - Necessity
      - Origin

    * - BPROTO_CI_DEPLOY_USER
      - Mandatory
      -

    * - BPROTO_CI_DEPLOY_TOKEN
      - Mandatory
      -

    * - BPROTO_CI_IMAGES_REGISTRY
      - Mandatory
      -

    * - *CI_COMMIT_REF_NAME*
      - Mandatory
      - GitLab

    * - *CI_DEFAULT_BRANCH*
      - Mandatory
      - GitLab

.. _pipelines-lang-docker-build-triggers:

Triggers
========

All jobs from *Docker "sub-pipeline"* stages are triggered and only if changes have been made to docker files.
Right now such files are detected by below patterns:

- ``conan/*``
- ``cmake/Conan.cmake``,
- ``docker/*.Dockerfile``,
- ``scripts/conan/``
- ``scripts/os/setup_*.sh``,
- ``requirements.txt``.

Stages overview
===============

Build stage
-----------

This stage is responsible for building `Docker <https://www.docker.com>`__ images from a clean state to make sure there are no unexpected results.

*ALLOW FAILURE*
    Jobs are not allowed to fail as any failure means fundamental problems in a specific docker image.

*ARTIFACTS*
    Jobs don't generate any artifacts.

*DEPENDENCIES*
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

*ENVIRONMENT*
    A list of all environment variables can be found :ref:`here <pipelines-lang-docker-environment>`.

*IMAGES*
    `docker:19.03.12 <https://hub.docker.com/_/docker>`__.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-docker-build-triggers>` is changed.

Deploy stage
------------

This stage is responsible for generating pre-build docker images for our pipelines.
So called pre-build images are crucial part of our CI/CD as it allows us to decrease execution time of jobs.

*ALLOW FAILURE*
    Jobs are not allowed to fail as these jobs are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

*ARTIFACTS*
    New docker images will be deployed to the project `container registry <https://docs.gitlab.com/ee/user/packages/container_registry/>`__.

*DEPENDENCIES*
    Jobs don't require artifacts from other jobs.

    Jobs has no dependencies from other stages or jobs.

*ENVIRONMENT*
    A list of all environment variables can be found :ref:`here <pipelines-lang-docker-environment>`.

*IMAGES*
    `docker:19.03.12 <https://hub.docker.com/_/docker>`__.

*TRIGGERS*
    Job is triggered only on ``CI_DEFAULT_BRANCH`` when any file from :ref:`trigger list <pipelines-lang-docker-build-triggers>` is changed.

Jobs overview
=============

.. table::
    :width: 100%
    :widths: 40 30 30

    +--------------------------------+--------+----------------------------------------------------+
    | Job name                       | Stage  | Image                                              |
    +================================+========+====================================================+
    | .. centered:: **General purpose images**                                                     |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:alpine3``       | build  | :ref:`Alipne 3 <images-general-alpine3>`           |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:alpine3``      | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:centos7``       | build  | :ref:`CentOS 7 <images-general-centos7>`           |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:centos7``      | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:centos8``       | build  | :ref:`CentOS 7 <images-general-centos8>`           |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:centos8``      | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:ubuntu2004``    | build  | :ref:`Ubuntu 20.04 <images-general-ubuntu2004>`    |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:ubuntu2004``   | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:ubuntu2304``    | build  | :ref:`Ubuntu 20.04 <images-general-ubuntu2304>`    |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:ubuntu2304``   | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | .. centered:: **Dedicated images**                                                           |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:clang-format``  | build  | :ref:`Clang-Format <images-dedicated-clangformat>` |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:clang-format`` | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:conan``         | build  | :ref:`Conan <images-dedicated-conan>`              |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:conan``        | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+
    | ``build:docker:hadolint``      | build  | :ref:`HadoLint <images-dedicated-hadolint>`        |
    +--------------------------------+--------+                                                    +
    | ``deploy:docker:hadolint``     | deploy |                                                    |
    +--------------------------------+--------+----------------------------------------------------+

Usage
=====

This is an internal part of the Docker "sub-pipeline" which meant to be used solely by :bproto:url:`3CT: Common CI/CD Tools` project.
