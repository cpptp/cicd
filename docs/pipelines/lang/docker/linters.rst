=======
Linters
=======

This is a "sub-pipeline" for linting new changes that have been made to `docker <https://www.docker.com>`__ files of a specific GitLab project.

.. _pipelines-lang-docker-linters-triggers:

Triggers
========

All jobs from *Docker "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to docker files.
Right now such files are detected by these patterns:

- ``**/.Dockerfile``.

Jobs overview
=============

check:docker:lint
-----------------

This job executes `hadolint <https://github.com/hadolint/hadolint>`__ *only on changed Docker files* (not diff, but files) which will diagnose bugs that can be deduced via static analysis.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.hadolint.yaml`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`hadolint:latest <images-dedicated-hadolint>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-docker-linters-triggers>` is changed.

Usage
=====

In order to use above CI jobs, you need to add the follow code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
