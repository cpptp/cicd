.. _pipelines-lang-cmake:

=====
CMake
=====

This is a "sub-pipeline" for linting new changes that have been made to CMake files of a specific GitLab project.

.. _pipelines-lang-cmake-triggers:

Triggers
========

All jobs from *CMake "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to CMake files.
Right now such files are detected by these patterns:

- ``*/CMakeLists.txt``,
- ``**/*.cmake``.

Jobs overview
=============

check:cmake:format
------------------

This job executes `cmake-format <https://github.com/cheshirekow/cmake_format>`__ *only on changed CMake files* (not diff, but files) which will reformat code accordingly to the established project code style.

.. note::
    This job is in active development phase and is a subject for a complete rework.
    Please follow :bproto:issue:`this project issue <5>` to get more information about the current state of this job.

*ALLOW FAILURE*
    .. warning:: This job is allowed to fail as it's still in development.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.cmake-format.py`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cmake-triggers>` is changed.

check:cmake:lint
----------------

This job executes `cmake-lint <https://github.com/cheshirekow/cmake_format>`__ *only on changed CMake files* (not diff, but files) which will diagnose and fix style violations like wrong variable and function names.

.. note::
    This job is in active development phase and is a subject for a complete rework.
    Please follow :bproto:issue:`this project issue <5>` to get more information about the current state of this job.

*ALLOW FAILURE*
    .. warning:: This job is allowed to fail as it's still in development.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.cmake-format.py`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cmake-triggers>` is changed.

Usage
=====

In order to use above CI jobs, you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
