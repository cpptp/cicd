============
Build & Test
============

This document describes a part of C++ "sub-pipeline" which is responsible for building and testing new changes that have been made to C++ files of a specific GitLab project.

Here we try to build project from a clean state on different OS's and with different set of build configurations to cover as much as possible.
Additionally to this we try to build project with all available sanitizers to detect different kind of issues as fast as possible.

Flowchart
=========

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/cpp_pipeline_flowchart.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_images/cpp_pipeline_flowchart.png
   :alt: C++ Pipeline Flowchart
   :class: only-light

.. image:: /_images/cpp_pipeline_flowchart.dark.png
   :alt: C++ Pipeline Flowchart
   :class: only-dark

Triggers
========

A list of triggers can be found :ref:`here <pipelines-lang-cpp-linters-triggers>`.

Stages overview
===============

Build stage
-----------

*ALLOW FAILURE*
    Jobs are not allowed to fail as any failure means project can't be fully assembled into distinct binary and library files.

*ARTIFACTS*
    Jobs generate C++ binaries for application executables and tests runners.

    Job artifacts also include all required CMake and CTest files for running tests.

*DEPENDENCIES*
    Jobs don't require artifacts from other jobs.

    Jobs have no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Allowed values
      - Default

    * - BPROTO_CI_CMAKE_ARGS
      - Mandatory
      -
      -

    * - BPROTO_CI_CMAKE_BUILD_TYPE
      - Optional
      - | Debug,
        | MinSizeRel,
        | Release,
        | RelWithDebInfo
      - Debug

    * - BPROTO_CI_CMAKE_EXTRA_ARGS
      - Optional
      -
      -

    * - CC
      - Mandatory
      - clang, gcc
      -

    * - CXX
      - Mandatory
      - clang++, g++
      -

    * - CMAKE_GENERATOR
      - Mandatory
      -
      - Unix Makefiles

.. note::

    The main difference between ``BPROTO_CI_CMAKE_ARGS`` and ``BPROTO_CI_CMAKE_EXTRA_ARGS`` is that the later can be used by an end-user to specializes additional CMake options.

.. warning::

    ``BPROTO_CI_CMAKE_ARGS`` must not be used by the end-user!

*IMAGES*
    All jobs from this stage must use pre-build docker images to decrease overall pipeline time.

*See also build stage overview from* :ref:`overview-stages-build-stage`.

Test stage
----------

This is a stage for running all available C++ tests.

*See also test stage overview from* :ref:`overview-stages-test-stage`.

Report stage
------------

This is a stage to collect all available build information during the *build* stage and deploy it as a job artifacts if build has failed by any reason.

*See also report stage overview from* :ref:`overview-stages-report-stage`.

Distribution
============

As we try to test as much configurations as possible, in this table all current setups will be tracked.

+-------------------+----------------------------------------------------------------------------+
| Options           | System Configuration                                                       |
+                   +-------------+-------------+----------------+-------------------+-----------+
|                   | CentOS 7    | *CentOS 8*  | *Ubuntu 20.04* | Ubuntu 23.04      | *Windows* |
+                   +-------------+-------------+----------------+-------------------+-----------+
|                   | GCC         | GCC         | GCC            | Clang   | GCC     | MSBuild   |
+===================+=============+=============+================+=========+=========+===========+
|                   | .. centered:: **Build Configuration**                                      |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Type              | **release** | **release** | **release**    | debug   | debug   | **both**  |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Asserts           | no          | no          | no             | **yes** | **yes** | **yes**   |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Cache             | default     | default     | default        | default | default | default   |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Linter            | default     | default     | default        | default | default | default   |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| LTO               | **yes**     | **yes**     | **yes**        | no      | **yes** | **yes**   |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
|                   | .. centered:: **Docs**                                                     |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Docs              | no          | no          | no             | **yes** | no      | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
|                   | .. centered:: **Linters**                                                  |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| LWYU              | **yes**     | **yes**     | **yes**        | **yes** | **yes** | **yes**   |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
|                   | .. centered:: **Sanitizers**                                               |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| ASan              | no          | no          | no             | no      | **yes** | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| LSan              | no          | no          | no             | no      | **yes** | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| MSan              | no          | no          | no             | no      | no      | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| UBSan             | no          | no          | no             | no      | **yes** | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| TSan              | no          | no          | no             | **yes** | no      | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Valgrind          | no          | no          | no             | no      | **yes** | no        |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
|                   | .. centered:: **Tests**                                                    |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
| Benchmarks        | .. centered:: **yes**                                                      |
+-------------------+                                                                            +
| Unit tests        |                                                                            |
+-------------------+                                                                            +
| Component tests   |                                                                            |
+-------------------+                                                                            +
| Integration tests |                                                                            |
+-------------------+                                                                            +
| System tests      |                                                                            |
+-------------------+-------------+-------------+----------------+---------+---------+-----------+
