=======
Linters
=======

This document describes a part of C++ "sub-pipeline" which is responsible for linting new changes that have been made to C++ files of a specific GitLab project.

Flowchart
=========

This is a very generalized flowchart that combines in itself all possible roots of pipeline generation and execution.

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/linters_pipeline_flowchart.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_images/linters_pipeline_flowchart.png
   :alt: Linters Pipeline Flowchart
   :class: only-light

.. image:: /_images/linters_pipeline_flowchart.dark.png
   :alt: Linters Pipeline Flowchart
   :class: only-dark

Triggers
========

Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cpp-linters-triggers>` is changed.

Jobs overview
=============

.. _manual_job: https://docs.gitlab.com/ee/ci/jobs/job_control.html#create-a-job-that-must-be-run-manually

.. _linters-pipeline-check-cpp-coverage:

check:cpp:coverage
------------------

This job calculates an overall coverage of C++ source code by tests.
In order to do that job builds project with compiler specific flags which allow to generate files that can be processed by `Gcov <https://en.wikipedia.org/wiki/Gcov>`__.

At this moment job doesn't reuse artifacts from *build* or *test* stage.
Even so it means pipeline could take much less time, it still much easy to manage a job by itself.

*ARTIFACTS*
    Job will generate a set of files suitable for generating an HTML report.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_CMAKE_ARGS
      - Mandatory

*IMAGES*
    :ref:`ubuntu2304:latest <images-general-ubuntu2304>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    As this job is a `manual one <manual_job>`_, it can be triggered only by a user.

report:cpp:coverage
-------------------

*ARTIFACTS*
    Job produces a statically generated HTML reports using `Gcovr <https://github.com/gcovr/gcovr>`__.

*DEPENDENCIES*
    Jobs require artifacts from :ref:`linters-pipeline-check-cpp-coverage` job.

*IMAGES*
    `alpine:3 <https://hub.docker.com/_/alpine>`__.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** and only if :ref:`linters-pipeline-check-cpp-coverage` job has failed.

.. _pipeline-lang-cpp-linters-cppcheck:

check:cpp:cppcheck
------------------

This job executes `cppcheck <https://github.com/danmar/cppcheck>`__ *only on changed C++ files* (not diff, but files) which detect bugs and focuses on detecting undefined behavior and dangerous coding constructs.

*ARTIFACTS*
    Job will generate a set of files suitable for generating an HTML report.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.cppcheck`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_CMAKE_GLOBAL_ARGS
      - Mandatory

*IMAGES*
    :ref:`ubuntu2304:latest <images-general-ubuntu2304>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    As this job is a `manual one <manual_job>`_, it can be triggered only by a user.

report:cpp:cppcheck
-------------------

*ARTIFACTS*
    Job produces a statically generated HTML reports using ``cppcheck-htmlreport`` from the `cppcheck <https://github.com/danmar/cppcheck>`__ package.

*DEPENDENCIES*
    Jobs require artifacts from :ref:`pipeline-lang-cpp-linters-cppcheck` job.

*IMAGES*
    `alpine:3 <https://hub.docker.com/_/alpine>`__.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** and only if :ref:`pipeline-lang-cpp-linters-cppcheck` job has failed.

.. _pipeline-lang-cpp-linters-format:

check:cpp:format
----------------

This job executes `clang-format <https://clang.llvm.org/docs/ClangFormat.html>`__ *only on changes from C++ files* which will reformat code accordingly to the established project code style.

.. warning::
    This job uses a docker image with a patched ``clang-format`` to support a custom C++ code style.

*ALLOW FAILURE*
    .. warning:: This job is allowed to fail as it's still in development.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    ``clang-format`` for each input file will try to find the ``.clang-format`` file located in the closest parent directory of the input file.
    Different configuration options can be found on `official page <https://clang.llvm.org/docs/ClangFormatStyleOptions.html>`__

    Default config, which can be found :repo:file:`here <.clang-format>` and shipped with project C++ related docker images, implements the `following formatting conventions <https://bproto.gitlab.io/dev-guides/cpp/naming.html>`__ and it's heavily opinionated.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_COMMIT_SHA
      - Mandatory
      - GitLab

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      -

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

*IMAGES*
    :ref:`clang-format:latest <images-dedicated-clangformat>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cpp-linters-triggers>` is changed.

check:cpp:include_guards
------------------------

This job executes ``checkguard`` from `guardonce <https://github.com/cgmb/guardonce>`__ package *only on changed C++ files* (not diff, but files) which will detect wrong written `include guards <https://en.wikipedia.org/wiki/Include_guard>`__.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Current config is hard-coded in :repo:file:`this file <scripts/cicd/lint/check_cpp_include_guards.sh>` and has the following value:

    - ``path | remove - | remove _ | prepend __BPROTO_ | append __ | upper``

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_PROJECT_DIR
      - Mandatory
      - GitLab

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      -

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cpp-linters-triggers>` is changed.

check:cpp:iostream
------------------

This job executes a simple shell script to find files that includes `iostream <https://en.cppreference.com/w/cpp/header/iostream>`__ library header and report them.

**Preface**

.. https://stackoverflow.com/questions/189562

.. epigraph::

   "The most effective debugging tool is still careful thought, coupled with judiciously placed print statements."

   -- Brian Kernighan, "Unix for Beginners" (1979)

``printf()`` debugging - useful and powerful technique. That said I often forget to remove redundant ``<iostream>`` header after debugging is done.

It might sound as a small issue however including the ``<iostream>`` header can significantly impact compile time, as it is one of the heaviest standard C++ headers.

**Suppressing warnings**

If you need to silence the warning, you can put ``// ignore`` comment on the same line as an ``include`` directive.

.. code-block:: cpp

   #include <iostream>  // ignore

*ARTIFACTS*
    Job doesn't generate any artifacts.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-cpp-linters-triggers>` is changed.

.. _pipeline-lang-cpp-linters-tidy:

check:cpp:tidy
--------------

This job executes `clang-tidy <https://clang.llvm.org/extra/clang-tidy>`__ *only on a diff* which will diagnose and fix typical programming errors, like style violations, interface misuse, or bugs that can be deduced via static analysis.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.clang-tidy`` name.

    Default config, which can be found :repo:file:`here <.clang-tidy>` and shipped with project C++ related docker images, implements the `following naming conventions <https://bproto.gitlab.io/dev-guides/cpp/naming.html>`__ and is rather restrictive.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      -

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      -

*IMAGES*
    :ref:`ubuntu2304:latest <images-general-ubuntu2304>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    As this job is a `manual one <manual_job>`_, it can be triggered only by a user.

check:cpp:tidy (nightly)
------------------------

This is almost the same job as :ref:`pipeline-lang-cpp-linters-tidy` with one slight difference.
This is a nightly job which checks entire code base on a daily bases.

The main reason for such separation is ability to detect a set of issues that is impossible to find while only checking files diff.
The downside of this solution is a long running job, which can take more than half an hour to run.

However that issue is mitigated a little bit by caching current job result and a commit hash on which that job has run.
So the next job run might do nothing if commit hash hasn't changed from the time of the last run.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

    Also it will generate and save a simple package (``nightly_clang_tidy``) that contains a result of current job run and a commit hash on which the job has run.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.clang-tidy`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`ubuntu2304:latest <images-general-ubuntu2304>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

Unsupported linters
===================

Here is a list of C++ linters that haven't been integrated yet.

**ClangBuildAnalyzer**
    Link: https://github.com/aras-p/ClangBuildAnalyzer

    Clang build analysis tool using ``-ftime-trace``.

**Clang-Include-Fixer**
    Link: https://clang.llvm.org/extra/clang-include-fixer.html

    One of the major nuisances of C++ compared to other languages is the manual management of ``#include`` directives in any file.
    ``clang-include-fixer`` addresses one aspect of this problem by providing an automated way of adding ``#include`` directives for missing symbols in one translation unit.

**Clang Static Analyzer**
    Link: https://clang-analyzer.llvm.org/

    The Clang Static Analyzer is a source code analysis tool that finds bugs in C, C++, and Objective-C programs.

**CodeChecker**
    Link: https://github.com/Ericsson/codechecker

    CodeChecker is an analyzer tooling, defect database and viewer extension for the Clang Static Analyzer and Clang-Tidy.

**flawfinder**
    Link: https://github.com/david-a-wheeler/flawfinder

    A static analysis tool for finding vulnerabilities in C/C++ source code.

**ikos**
    Link: https://github.com/NASA-SW-VnV/ikos

    Static analyzer for C/C++ based on the theory of Abstract Interpretation.

**Infer**
    Links: https://github.com/facebook/infer

    A static analyzer for Java, C, C++, and Objective-C.

**Klocwork**
    Link: https://www.perforce.com/products/klocwork

    Klocwork static code analysis and SAST tool for C, C++, C#, Java, JavaScript, Python, and Kotlin identifies software security, quality, and reliability issues helping to enforce compliance with standards.

**krazy**
    Link: https://github.com/Krazy-collection/krazy

    An extensible framework for performing static code checking on files of any format.

**OCLint**
    Link: https://github.com/oclint/oclint

    A static source code analysis tool to improve quality and reduce defects for C, C++ and Objective-C.

**stack**
    Link: https://css.csail.mit.edu/stack

    An analyzer that tries to report code that might be removed by an optimizer.
