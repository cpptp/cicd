.. _pipelines-lang-cpp:

===
C++
===

This is a "sub-pipeline" for building, testing and linting new changes that have been made to C++ files of a specific GitLab project.

.. toctree::
   :maxdepth: 1
   :hidden:

   build.rst
   linters.rst

:doc:`build`
    A document which covers all CI jobs responsible for building and testing C++ code.

:doc:`linters`
    A document which covers all CI jobs responsible for linting C++ code.

.. _pipelines-lang-cpp-limitations:

Limitations
===========

Unlike other "sub-pipeline" like :ref:`CMake <pipelines-lang-cmake>`, :ref:`Python <pipelines-lang-python>` or :ref:`Shell <pipelines-lang-shell>` ones, current implementation of C++ "sub-pipeline" is hard to use as a template because it has a few significant limitations.

Core packages
^^^^^^^^^^^^^

Everything here is built using `CMake <https://cmake.org>`__ build system and `Conan <https://conan.io/>`__ package manager.

It's still possible to integrate other build systems (like `Bazel <https://bazel.build/>`__, `b2 <https://build2.org>`__, `Meson <https://mesonbuild.com>`__ and `Premake <https://premake.github.io>`__) and package managers (like `CPM <https://github.com/cpm-cmake/CPM.cmake>`__ and `vcpkg <https://vcpkg.io>`__)!
However as of now I have no plans to do that because I use nothing from that and that will require a significant amount of work.

CMake modules
^^^^^^^^^^^^^

Most jobs heavily relay on `CMOPTS <https://gitlab.com/bproto/cmopts>`__ and `CppDoc <https://gitlab.com/bproto/cppdoc>`__ CMake modules.

Those modules do all the heavy lifting like a seamless integration of sanitizes, linters and other useful tools, hence are essential for this "sub-pipeline".

Pre-build Docker images
^^^^^^^^^^^^^^^^^^^^^^^

Due to the fact that all "sub-pipeline"s (this one is not an exception) try to utilizes :ref:`pre-build docker images <images>` as much as possible, this will be the rock on which we split in most of the cases.

.. _gitlab-caching: https://docs.gitlab.com/ee/ci/caching
.. _gitlab-fork: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

There is no easy way to add new `conan packages <https://conan.io>`__ to mentioned docker images so you need either to use `GitLab caching mechanism <gitlab-caching_>`_ to some degree or `fork <gitlab-fork_>`_ the project to handle that.

Project structure
^^^^^^^^^^^^^^^^^

Most jobs, from the current implementation of the C++ "sub-pipeline", heavily relay on a certain C++ project structure that is shown below.

.. Generated with the help of https://tree.nathanfriend.io.

.. _"check:cpp:format": pipeline-lang-cpp-linters-format

.. parsed-literal::

   :olive:`green` - is an optional file or directory.
   :red:`red` - is a mandatory file or directory.

   my-app/
   :olive:`├── 3rd-party/` :gray:`<- code here is ignored by linters.`
   │   ├── 3rd-party-cpp-project-1/
   │   │   └── ...
   │   ├── 3rd-party-cpp-project-2/
   │   │   └── ...
   │   └── CMakeLists.txt
   :olive:`├── docs/` :gray:`<- docs job looks for changes here.`
   │   ├── .gitignore
   │   ├── CMakeLists.txt
   │   └── index.rst
   :red:`├── project/` (:bproto:issue:`issue <7>`) :gray:`<- 'check:cpp:format' formats code only here.`
   │   ├── module-1/
   │   │   :olive:`├── docs/` :gray:`<- docs job looks for changes here.`
   │   │   │   ├── api.rst
   │   │   │   ├── developer_notes.rst
   │   │   │   └── index.rst
   │   │   :olive:`├── include/` :gray:`<- docs job looks for changes here.`
   │   │   │   └── module-1/
   │   │   │       ├── module-1-api-header-1.hpp
   │   │   │       └── ...
   │   │   ├── mocks/
   │   │   │   └── misc-project-1/
   │   │   │       ├── module-1-mock-header-1.hpp
   │   │   │       └── ...
   │   │   ├── src/
   │   │   │   └── ...
   │   │   ├── tests/
   │   │   │   └── ...
   │   │   └── CMakeLists.txt
   │   ├── module-2/
   │   │   └── ...
   │   :olive:`├── misc/` (:bproto:issue:`issue <6>`) :gray:`<- 'check:cpp:include_guards' handles this folder in a special way.`
   │   │   ├── misc-module-1/
   │   │   │   └── ...
   │   │   ├── mist-project-2/
   │   │   │   └── ...
   │   │   └── CMakeLists.txt
   │   └── CMakeLists.txt
   :olive:`├── tools/` :gray:`<- sanitizers look for files with suppressions only here.`
   │   ├── lsan.supp
   │   └── valgrind.supp
   ├── .gitignore
   ├── .gitlab-ci.yml
   :red:`├── CMakeLists.txt` :gray:`<- most jobs expect this file to be here.`
   :olive:`├── conanfile.{py,txt}` :gray:`<- most jobs will use it if exists.`
   ├── README.md
   └── requirements.txt

.. _pipelines-lang-cpp-linters-triggers:

Triggers
========

Most of jobs from *C++ "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to C++ files.
Right now such files are detected by these patterns:

- ``project/**/*.cpp``,
- ``project/**/*.hpp``,
- ``project/**/*.ipp``.

Usage
=====

In order to use this "sub-pipeline" you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/cpp.yml'
