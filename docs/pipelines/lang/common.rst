.. _pipelines-lang-common:

======
Common
======

This is a "sub-pipeline" for linting new changes that have been made to any kind of files of a specific GitLab project.

Triggers
========

All jobs from *common "sub-pipeline"* stages are triggered only on merge requests if changes have been made to any file in git repository.

Jobs overview
=============

check:code:license
------------------

This job executes `licenseheaders <https://github.com/johann-petrak/licenseheaders>`__ on project source code files, which will detect incorrect license headers.

.. note::
    This job is in active development phase and is a subject for a complete rework.
    Please follow :bproto:issue:`this project issue <9>` to get more information about the current state of this job.

*ALLOW FAILURE*
    .. warning:: This job is allowed to fail as it's still in development.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.license.tmpl`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job will be triggered only on **merge requests** and on all project files except ``3rd-party`` folder.

check:code:spelling
-------------------

This job executes `codespell <https://github.com/codespell-project/codespell>`__ on project source code files, which will detect common misspellings.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.codespellrc`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job will be triggered only on **merge requests** and on all project files except ``3rd-party`` folder.

check:commits:spelling
----------------------

This job performs various checks on commit messages for all found branch commits.

Below is a list of implemented checks.

- Check for common misspellings.
- Check that subject and body don't exceed 80 symbols limit.
- Check that GitHub issue references list follows ``GitHub: #1, #2, ...`` format.
- Check that GitHub issue references list follows ``GitLab: #1, #2, ...`` format.
- Check that JIRA issues list follows ``Jira: PROJ-1, PROJ-2, ...`` format.
- Check that tags list follows ``Tags: Bar, Foo, ...`` format.

Here is an example of a valid message:

    | Fix foo to enable bar
    |
    | This fixes the broken behavior of the component by doing xyz.
    |
    | GitHub: #42, #43
    | GitLab: #315, #316
    | Jira: PROJ-101
    | Tags: Bar, Foo

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.codespellrc`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Origin

    * - CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      - Mandatory
      - GitLab

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job will be triggered only on **merge requests**.

Usage
=====

In order to use above CI jobs, you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
