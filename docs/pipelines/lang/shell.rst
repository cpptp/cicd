.. _pipelines-lang-shell:

=====
Shell
=====

This is a "sub-pipeline" for linting new changes that have been made to shell scripts of a specific GitLab project.

.. _pipelines-lang-shell-triggers:

Triggers
========

All jobs from *shell "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to shell scripts.
Right now such files are detected by these patterns:

- ``**/*.sh``.

Jobs overview
=============

check:shell:format
------------------

This job executes `shfmt <https://github.com/mvdan/sh>`__ *only on changed Shell files* (not diff, but files) which will diagnose and fix style violations, or bugs that can be deduced via static analysis.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.editorconfig`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Type

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      - Public

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      - Internal

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-shell-triggers>` is changed.

check:shell:lint
----------------

This job executes `shellcheck <https://github.com/koalaman/shellcheck>`__ *only on changed Shell files* (not diff, but files) which will diagnose and fix style violations like wrong variable and function names.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.shellcheck`` name.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity
      - Type

    * - BPROTO_CI_LINTER_TRACE
      - Optional
      - Public

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory
      - Internal

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-shell-triggers>` is changed.

Usage
=====

In order to use above CI jobs, you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
