============
Build & Test
============

There is no CI jobs in current CI/CD implementation, but there is couple of code snippets that shows how easy it to create a CI job to build and test python code.

Global cache
============

.. code-block:: yaml

   variables:
     PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

   cache:
     key:
       files:
         - requirements.txt
     paths:
       - .cache/pip

   example:
     image: python:3-alpine3.15
     stage: build-test
     before_script:
       - pip install -r requirements.txt
     script:
       # build a package
       - pip install .
       # or run tests
       - pytest

Job's cache
===========

.. code-block:: yaml

   example:
     image: python:3-alpine3.15
     stage: build-test
     variables:
       PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
     before_script:
       - pip install -r requirements.txt
     script:
       # build a package
       - pip install .
       # or run tests
       - pytest
     cache:
       key:
         files:
           - requirements.txt
       paths:
         - .cache/pip
