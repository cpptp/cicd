=======
Linters
=======

This is a "sub-pipeline" for linting new changes that have been made to python files of a specific GitLab project.

.. _pipelines-lang-python-linters-triggers:

Triggers
========

All jobs from *python "sub-pipeline"* stages are triggered only on merge requests and only if changes have been made to python files.
Right now such files are detected by these patterns:

- ``**/*.py``.

Jobs overview
=============

check:python:black
------------------

This job executes `black <https://github.com/psf/black>`__ *only on changed python files* (not diff, but files) which will diagnose and fix style violations.

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-python-linters-triggers>` is changed.

check:python:flake8
-------------------

This job executes `flake8 <https://github.com/PyCQA/flake8>`__ *only on changed python files* (not diff, but files) which will diagnose and fix style violations, or bugs that can be deduced via static analysis.

*ARTIFACTS*
    Job will generate a simple log file with all found issues.

*CONFIG*
    Configuration file is located at the root of the repository and has ``.flake8`` name.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-python-linters-triggers>` is changed.

check:python:isort
------------------

This job executes `isort <https://pycqa.github.io/isort>`__ *only on changed python files* (not diff, but files) which will diagnose and fix issues related to python imports.

Different configuration options can be found on `official page <https://pycqa.github.io/isort/docs/configuration/options.html>`__

*ARTIFACTS*
    Job will generate a patch file with corrected lines.

*CONFIG*
    ``isort`` for each input file will try to find the ``.isort.cfg`` file located in the closest parent directory of the input file.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1

    * - Name
      - Necessity

    * - BPROTO_CI_LINTER_TRACE
      - Optional

    * - BPROTO_CI_MERGE_REQUEST_CHANGED_FILES
      - Mandatory

*IMAGES*
    :ref:`alpine:3 <images-general-alpine3>`.

*STAGE*
    :ref:`overview-stages-check-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-lang-python-linters-triggers>` is changed.
