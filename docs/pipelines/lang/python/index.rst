.. _pipelines-lang-python:

======
Python
======

This is a "sub-pipeline" for building, testing and linting new changes that have been made to `python <https://www.python.org>`__ files of a specific GitLab project.

.. toctree::
   :maxdepth: 1
   :hidden:

   build.rst
   linters.rst

:doc:`build`
    A document which covers all CI jobs responsible for building and testing python code.

:doc:`linters`
    A document which covers all CI jobs responsible for linting python code.

Usage
=====

In order to use this "sub-pipeline", you need to add the following code to your `.gitlab-ci.yml`:

.. code-block:: yaml

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/lite.yml'
