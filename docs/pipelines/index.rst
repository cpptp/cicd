=============
Sub-Pipelines
=============

These documents cover in-detail all available "sub-pipelines" and their jobs.

.. toctree::
   :maxdepth: 1
   :hidden:

   docs/index.rst
   lang/index.rst
   misc.rst
   packages/index.rst

Concept
=======

We always try to compose everything into one top-level `GitLab CI/CD pipeline <https://docs.gitlab.com/ee/ci/pipelines/>`__ that includes all jobs.
This way we can have all `GitLab CI/CD jobs <https://docs.gitlab.com/ee/ci/jobs/>`__, that have been triggered for merge request or a specific branch, in one place.

That said, we still try to decompose a top-level pipeline into smaller ones ("sub-pipelines").
Such approach decrease cognitive overload and makes it much easy to understand certain aspects of our pipeline.

Below table ("map") shows how jobs are distributed across different "sub-pipelines".

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/jobs_map.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_images/jobs_map.png
   :alt: Map of Sub-Pipelines Jobs
   :class: only-light

.. image:: /_images/jobs_map.dark.png
   :alt: Map of Sub-Pipelines Jobs
   :class: only-dark

List
====

:doc:`docs/index`
    A collection "sub-pipeline"s for building and deploying documentation.

:doc:`lang/index`
    A collection of "sub-pipeline"s for building, testing and linting files related to different programming languages.

:doc:`misc`
    A "sub-pipeline" with different miscellaneous CI jobs.

:doc:`packages/index`
    A collection "sub-pipeline"s for building and deploying different packages.
