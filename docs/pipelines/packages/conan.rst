.. _pipelines-packages-conan:

=====
Conan
=====

This is a "sub-pipeline" for building, testing and deploying `conan packages <https://conan.io/>`__.

.. _pipelines-packages-conan-triggers:

Triggers
========

All jobs from docker pipeline stages are triggered only if changes have been made to *conan recipe* related files.
Right now such files are detected by these patterns:

- ``conandata.yml``,
- ``conanfile.py``.

**See also:** `Conan packages in the GitLab Package Registry <https://docs.gitlab.com/ee/user/packages/conan_repository>`__.

Jobs overview
=============

build:conan
-----------

This stage is responsible for building conan packages from a clean state to make sure there are no unexpected results.

*ALLOW FAILURE*
    Job is not allowed to fail as any failure here means fundamental problems in a specific conan recipe.

*ARTIFACTS*
    Job doesn't generate any artifacts.

*DEPENDENCIES*
    Job doesn't require artifacts from other jobs and has no dependencies from other stages or jobs.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1
    :width: 100%

    * - Name
      - Necessity
      - Origin

    * - BPROTO_CI_DEPLOY_USER
      - Mandatory
      -

    * - BPROTO_CI_DEPLOY_TOKEN
      - Mandatory
      -

    * - BPROTO_CI_CONAN_PACKAGE_NAME
      - Mandatory
      -

    * - BPROTO_CI_CONAN_PACKAGES_REGISTRY
      - Mandatory
      -

    * - *CI_COMMIT_REF_NAME*
      - Mandatory
      - GitLab

    * - *CI_DEFAULT_BRANCH*
      - Mandatory
      - GitLab

    * - *CI_PROJECT_ROOT_NAMESPACE*
      - Mandatory
      - GitLab

*IMAGES*
    :ref:`conan:latest <images-dedicated-conan>`.

*STAGE*
    :ref:`overview-stages-build-stage`.

*TRIGGERS*
    Job is triggered only on **merge requests** when any file from :ref:`trigger list <pipelines-packages-conan-triggers>` is changed.

deploy:conan
------------

.. _package registry: https://docs.gitlab.com/ee/user/packages/package_registry

This stage is responsible for deploying conan packages to the `package registry`_ defined in ``gitlab`` `remote <https://docs.conan.io/1/reference/commands/misc/remote.html>`__ (which should usually be equal to :ref:`BPROTO_CI_CONAN_PACKAGES_REGISTRY <variables-bproto-ci-conan-package-name>` environment variable).

*ALLOW FAILURE*
    Job is not allowed to fail as these jobs are run only on ``CI_DEFAULT_BRANCH`` after accepting a successful merge request!

*ARTIFACTS*
    New conan package will be deployed to the project `package registry`_.

*DEPENDENCIES*
    Job doesn't generate any artifacts.

*ENVIRONMENT*

.. list-table::
    :header-rows: 1
    :width: 100%

    * - Name
      - Necessity
      - Origin

    * - BPROTO_CI_DEPLOY_USER
      - Mandatory
      -

    * - BPROTO_CI_DEPLOY_TOKEN
      - Mandatory
      -

    * - BPROTO_CI_CONAN_PACKAGE_NAME
      - Mandatory
      -

    * - BPROTO_CI_CONAN_PACKAGES_REGISTRY
      - Mandatory
      -

    * - *CI_COMMIT_REF_NAME*
      - Mandatory
      - GitLab

    * - *CI_DEFAULT_BRANCH*
      - Mandatory
      - GitLab

    * - *CI_PROJECT_ROOT_NAMESPACE*
      - Mandatory
      - GitLab

*IMAGES*
    :ref:`conan:latest <images-dedicated-conan>`.

*STAGE*
    :ref:`overview-stages-deploy-stage`.

*TRIGGERS*
    Job is triggered only on ``CI_DEFAULT_BRANCH`` when any file from :ref:`trigger list <pipelines-packages-conan-triggers>` is changed.

Usage
=====

To correctly use provided CI jobs you need to set :ref:`BPROTO_CI_CONAN_PACKAGES_REGISTRY <variables-bproto-ci-conan-package-name>` either in `project CI/CD settings <https://docs.gitlab.com/ee/ci/variables/#for-a-project>`__ or as shown below.

.. code-block:: yaml

   variables:
     BPROTO_CI_CONAN_PACKAGE_NAME: "test_package"

   include:
     - project: 'bproto/cicd'
       ref: main
       file:
         - '/.gitlab/ci/external/conan.yml'
