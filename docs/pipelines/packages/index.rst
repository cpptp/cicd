========
Packages
========

These documents cover in-detail all available "sub-pipelines" for generating different type of packages.

.. toctree::
   :maxdepth: 1
   :hidden:

   conan.rst

:doc:`conan`
    A GitLab CI/CD "sub-pipeline" which covers all CI jobs responsible for building and deploying conan packages.
