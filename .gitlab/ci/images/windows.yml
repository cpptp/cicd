# Copyright 2023-2024 Bytes Mess <b110011@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

.windows_changes: &windows_changes
  - conan/conanfile.txt
  - docker/Windows.Dockerfile
  - scripts/cicd/helpers/*.ps1
  - pyproject.toml

.build-windows-base-docker:
  tags:
    - shared-windows
    - windows
    - windows-1809
  timeout: 2h
  variables:
    DOCKER_FILE: "docker/Windows.Dockerfile"
    DOCKER_IMAGE_NAME: windows-base:latest
    LATEST_IMAGE: "${BPROTO_CI_IMAGES_REGISTRY}/${DOCKER_IMAGE_NAME}"
  before_script:
    # https://forum.gitlab.com/t/83245
    - Start-Service -Name "docker"
  script:
    - docker login -u "${BPROTO_CI_DEPLOY_USER}" -p "${BPROTO_CI_DEPLOY_TOKEN}" "${CI_REGISTRY}"
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4480
    - (docker image pull "${LATEST_IMAGE}")
    - |-
        docker build `
          --pull `
          --cache-from "${LATEST_IMAGE}" `
          --tag "${DOCKER_IMAGE_NAME}" `
          --target base `
          --file "${DOCKER_FILE}" `
          .
    # Push only on default branch, otherwise only check if image can be successfully built.
    - |-
        if ("${CI_COMMIT_REF_NAME}" -eq "${CI_DEFAULT_BRANCH}") {
          docker tag "${DOCKER_IMAGE_NAME}" "${LATEST_IMAGE}"
          docker push "${LATEST_IMAGE}"
        }

.build-windows-docker:
  tags:
    - shared-windows
    - windows
    - windows-1809
  timeout: 2h
  variables:
    DOCKER_FILE: "docker/Windows.Dockerfile"
    DOCKER_IMAGE_NAME: windows:latest
    LATEST_BASE_IMAGE: "${BPROTO_CI_IMAGES_REGISTRY}/windows-base:latest"
    LATEST_IMAGE: "${BPROTO_CI_IMAGES_REGISTRY}/${DOCKER_IMAGE_NAME}"
  before_script:
    - Start-Service -Name "docker"
  script:
    - docker login -u "${BPROTO_CI_DEPLOY_USER}" -p "${BPROTO_CI_DEPLOY_TOKEN}" "${CI_REGISTRY}"
    # Fail if there is no base image, as we won't fit in 2 hours.
    - docker image pull "${LATEST_BASE_IMAGE}"
    - |-
        docker build `
          --pull `
          --cache-from "${LATEST_BASE_IMAGE}" `
          --cache-from "${LATEST_IMAGE}" `
          --tag "${DOCKER_IMAGE_NAME}" `
          --target conan-pkgs `
          --file "${DOCKER_FILE}" `
          .
    - |-
        if ("${CI_COMMIT_REF_NAME}" -eq "${CI_DEFAULT_BRANCH}") {
          docker tag "${DOCKER_IMAGE_NAME}" "${LATEST_IMAGE}"
          docker push "${LATEST_IMAGE}"
        }

.windows-docker-builder:
  stage: build
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        *windows_changes
      when: manual

.windows-docker-deployer:
  stage: deploy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: manual
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        *windows_changes
      when: manual

build:docker:windows-base:
  extends:
    - .build-windows-base-docker
    - .windows-docker-builder

build:docker:windows:
  extends:
    - .build-windows-docker
    - .windows-docker-builder

deploy:docker:windows-base:
  extends:
    - .build-windows-base-docker
    - .windows-docker-deployer

deploy:docker:windows:
  extends:
    - .build-windows-docker
    - .windows-docker-deployer
