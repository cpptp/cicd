# GitLab Common CI/CD Tools

[fork]: https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

**GitLab Common CI/CD Tools** (**GitLab 3CT** for short) provides a [GitLab CI/CD](https://docs.gitlab.com/ee/ci) solution that helps to control the quality of mainly C++ and Python codebases and their surrounding infrastructure.

While the driving force of this module is to reduce duplication of CI/CD configurations and scripts across projects developed by me ([Bytes Mess](https://gitlab.com/b110011)), it is intended to be useful for any software that wants to utilize GitLab CI/CD to some degree.

This module also may serve as a useful reference for setting up a GitLab CI/CD using good practices.

**Table of contents** (generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/))

- [Usage](#usage)
  - [CI/CD templates](#cicd-templates)
  - [Forks](#forks)
- [License](#license)

## Usage

By design this project is intended to be used as a [CI/CD template](https://docs.gitlab.com/ee/ci/examples/index.html) in most of the cases.
However due to some limitations it might be [forked][fork] to bring some changes and then to be used as your own CI/CD template.
Both ways will be covered in-deep down on this page.

### CI/CD templates

This project provides a bunch of useful [GitLab CI/CD pipelines](https://docs.gitlab.com/ee/ci/pipelines/index.html) for different programming languages and frameworks.
Below is a nice list of all available pipelines:

[conan-tmpl]: .gitlab/ci/external/conan.yml
[cpp-tmpl]: .gitlab/ci/external/cpp.yml
[hugo-tmpl]: .gitlab/ci/external/hugo.yml
[lite-tmpl]: .gitlab/ci/external/conan.yml
[sphinx-tmpl]: .gitlab/ci/external/sphinx.yml

- **Documentation**
  - Hugo ([.gitlab/ci/external/hugo.yml][hugo-tmpl])
  - Sphinx ([.gitlab/ci/external/sphinx.yml][sphinx-tmpl])
- **Languages**
  - C++ ([.gitlab/ci/external/cpp.yml][cpp-tmpl])
  - CMake ([.gitlab/ci/external/lite.yml][lite-tmpl])
  - Docker ([.gitlab/ci/external/lite.yml][lite-tmpl])
  - Linux shell ([.gitlab/ci/external/lite.yml][lite-tmpl])
  - Python ([.gitlab/ci/external/lite.yml][lite-tmpl])
  - YAML ([.gitlab/ci/external/lite.yml][lite-tmpl])
- **Packages**
  - Conan ([.gitlab/ci/external/conan.yml][conan-tmpl])

You can start using those pipelines by including one of them as shown below:

```yaml
include:
  - project: 'bproto/cicd'
    ref: main
    file:
      - '/.gitlab/ci/external/lite.yml'
```

You also need to define below [CI/CD variables](https://docs.gitlab.com/ee/ci/variables) in your [group](https://docs.gitlab.com/ee/ci/variables/#for-a-group) or [project](https://docs.gitlab.com/ee/ci/variables/#for-a-project) CI/CD settings.

| Type     | Key                       | Value                                  | Protected | Masked  | Environments |
| ---      | ---                       | ---                                    | ---       | ---     | ---          |
| Variable | BPROTO_CI_IMAGES_REGISTRY | `registry.gitlab.com/bproto/artifacts` | no        | no      | All          |

Now you are ready to go!

[overview]: https://bproto.gitlab.io/cicd/overview/index.html
[overview-stages]: https://bproto.gitlab.io/cicd/overview/stages.html
[overview-workflow]: https://bproto.gitlab.io/cicd/overview/workflow.html

> To have a better understanding of pipelines implementation, you might also consider looking at an [overview][overview] of different aspects of pipelines, such as a list of implemented [stages][overview-stages] and how the [workflow][overview-workflow] was designed.

### Forks

[forking]: https://bproto.gitlab.io/cicd/fork.html

If you think, you need to fork this project and adjust it to your personal needs, please consider taking a look at [this documentation page][forking].

## License

All code is licensed under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
